// Sort data berdasarkan score yang paling besar -> kecil
// kalau ada nilai yang sama, sort berdasarkan huruf pertama di nama.
//  tidak boleh pakai .sort()

function highestScore (students) {
    // Code disini
      let tempArray= [];
  
      for (let i = 0; i < students.length; i++) {
        for (let j = 0; j < students.length; j++) {
  
          if (students[j].score < students[i].score) {
            tempArray = students[j];
            students[j] = students[i];
            students[i] = tempArray;
          } 
          
          else if (students[j].score == students[i].score) {
            if (students[j].name > students[i].name) {
              tempArray = students[j];
              students[j] = students[i];
              students[i] = tempArray;
            }
          }
          
        }
      } return students;
     
    }
    
    // TEST CASE
    console.log(highestScore([
      {
        name: 'agus',
        score: 90,
      },
      {
        name: 'agung',
        score: 85,
        class: 'wolves'
      },
      {
        name: 'ahmad',
        score: 85,
      },
      {
        name: 'anas',
        score: 78,
        class: 'wolves'
      }
    ]));
    
    console.log(highestScore([
      {
        name: 'alex',
        score: 100,
      },
      {
        name: 'irwin',
        score: 92,
        class: 'wolves'
      },
      {
        name: 'eri',
        score: 92,
      },
      {
        name: 'irsan',
        score: 92,
        class: 'wolves'
      },
      {
        name: 'umar',
        score: 80,
        class: 'tigers'
      }
    ]));
    
    
  